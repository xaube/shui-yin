#include "Game.hpp"
#include "TextureManager/TextureManager.hpp"

// Temporary player object
SDL_Rect clip[7];
SDL_Rect pos = {0, 0, 96*3, 96*3};
SDL_Texture *playerTexture;
int count = 0; // Frame count
// ---

Game::Game(){
    running = false;
    gRenderer = nullptr;
    gWindow = nullptr;
    
    for(int i = 0; i < 7; i++){
        clip[i] = { i*96, 0, 96, 96 }; 
    }
    for(int i = 0; i < 7; i++){
        std::cout << clip[i].x << std::endl;
    }
}

// Initializing SDL
void Game::init(const char* wn, int sw, int sh){
    if(SDL_Init(SDL_INIT_VIDEO) != 0){
        std::cout << "[ERROR] Failed to initialize SDL" << std::endl;
    }else{
        gWindow = SDL_CreateWindow(wn, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, sw, sh, SDL_WINDOW_SHOWN);
        if(gWindow == nullptr){
            std::cout << "[ERROR] Failed to create window" << std::endl;
        }else{
            gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
            int imgFlags = IMG_INIT_PNG;
            if(!(IMG_Init(imgFlags) &imgFlags)){
                std::cout << "Failed to initialize SDL_image" << std::endl;
            }
            if(gRenderer == nullptr){
                std::cout << "[ERROR] Failed to create renderer" << std::endl;
            }else{
                running = true;
                std::cout << "[INFO] Game initialized successfully!" << std::endl;
            }
        }
    }
    playerTexture = TextureManager::Load_Texture(gRenderer, "/home/agnex/Dev/shui-yin/media/player/player_idle_spritesheet.png");
}

bool Game::is_running(){ return running; }

void Game::handle_input(){
    if(SDL_PollEvent(&e) != 0){
        if(e.type == SDL_QUIT){
            running = false;
        }else if(e.type == SDL_KEYDOWN){
            switch(e.key.keysym.sym){
                case SDLK_d:
                    pos.x += 10;
                break;
            }            
        }
    }
}

void Game::update(){
    count++;
    if(count/6 >= 7){
        count = 0;
    }
}

void Game::render(){
    SDL_SetRenderDrawColor(gRenderer, 225, 0, 0, 0);
    SDL_RenderClear(gRenderer);
    SDL_RenderCopy(gRenderer, playerTexture, &clip[count/6], &pos);
    SDL_RenderPresent(gRenderer);
}

void Game::quit(){
    SDL_Quit();
}

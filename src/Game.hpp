#pragma once
#include<SDL2/SDL.h>
#include<SDL2/SDL_image.h>
#include<iostream>

class Game{
public:
    Game();
    void init(const char*, int, int);
    bool is_running();
    void handle_input();
    void update();
    void render();
    void quit();
private:
    bool running;
    SDL_Event e;
    SDL_Renderer *gRenderer;
    SDL_Window *gWindow;
};

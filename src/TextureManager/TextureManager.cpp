#include "TextureManager.hpp"

SDL_Texture *TextureManager::Load_Texture(SDL_Renderer *r, const char* path){
    SDL_Texture *optTex = nullptr;
    SDL_Surface *tmpSurf = IMG_Load(path);
    optTex = SDL_CreateTextureFromSurface(r, tmpSurf);
    SDL_FreeSurface(tmpSurf);
    if(optTex == nullptr){
        std::cout << "There is a problem!" << std::endl;
    }
    return optTex;
}

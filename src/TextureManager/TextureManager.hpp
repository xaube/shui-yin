#pragma once
#include "./../Game.hpp"

class TextureManager{
public:
    static SDL_Texture *Load_Texture(SDL_Renderer *, const char *);
};

#include"Game.hpp"

Game *game;
const int FPS = 60;
int timeDelay = 1000/FPS;

int main(int argc, char* argv[]){
    game = new Game();
    game->init("Protótipo", 1280, 720);
    Uint32 timeStart;
    int timeEnd;
    while(game->is_running()){
        timeStart = SDL_GetTicks();

        game->handle_input();
        game->update();
        game->render();
        
        // Frame capping
        timeEnd = SDL_GetTicks()-timeStart;
        if(timeEnd < timeDelay){
            SDL_Delay(timeDelay-timeEnd);
        }
    }
    game->quit();
    return 0;
}

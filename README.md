# Installation Instructions
To be able to play the game you need to have two packages.
You can download both from the Ubuntu official repository.

- libsdl2-2.0-0
- libsdl-image-2-2.0-0

That's it!

# Compile Instructions
Using a terminal, go into the directory where is located the Makefile, then just type make.
Also, you will need the following packages:

- libsdl2-dev
- libsdl2-image-dev
